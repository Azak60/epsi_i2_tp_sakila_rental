package Reducer;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


public class MovieRatingReducer extends Reducer<Text, Text, Text, Text> {
	@Override
	protected void reduce(Text title, Iterable<Text> ratingList, Reducer<Text, Text, Text, Text>.Context context) throws IOException, InterruptedException {
		// Utilisation du long pour optimiser la mémoire car beaucoup d'enregistrement possible
		long ageSum = 0;
		long ageNum = 0;
		
		// Pas d'utilisation de 'new' ce qui permet de ne pas créer des objets lourds en mémoire
		for(Text rate : ratingList) {
			ageSum += age.get();
			ageNum ++;
		}
		
		context.write(firstName,  new IntWritable((int)(ageSum / ageNum)));
	}
}
