package Reducer;
import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


public class LocationRatingReducer extends Reducer<Text, FloatWritable, Text, FloatWritable> {
	@Override
	protected void reduce(Text rating, Iterable<FloatWritable> amountList, Reducer<Text, FloatWritable, Text, FloatWritable>.Context context) 
			throws IOException, InterruptedException {

		float amountResult = 0;
		
		for(FloatWritable amount : amountList) {
			amountResult += amount.get();
		}
		
		context.write(rating,  new FloatWritable(amountResult));
	}
}
