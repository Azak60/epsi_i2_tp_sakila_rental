package Driver;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import Mapper.LocationClientMapper;
import Reducer.LocationClientReducer;

public class LocationClientDriver extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {
		// Lorsqu'on suivra le MapReduce, on verra dans les outils de supervision les données avec la marque "AgeAvgByName"
		Job job = Job.getInstance(this.getConf(), "amountLocationByClient");
		
		job.setJarByClass(this.getClass()); 					// Quel JAR doit être utilisé pour le Map Reduce ?
		job.setMapperClass(LocationClientMapper.class); 			// Quelle classe pour le Mapper ?
		job.setReducerClass(LocationClientReducer.class); 	// Quelle classe pour le Reducer ?
		job.setMapOutputKeyClass(Text.class); 					// Quel type de sortie du Mapper/Reducer pour la clé
		job.setMapOutputValueClass(FloatWritable.class); 			// Quel type de sortie du Mapper/Reducer pour la valeur
		job.setInputFormatClass(TextInputFormat.class);			// Format d'entrée du Mapper
		job.setOutputFormatClass(TextOutputFormat.class); 		// Format de sortie du Reducer
		FileInputFormat.addInputPath(job,  new Path(args[0])); // Où sont les fichiers d'entrée
		FileOutputFormat.setOutputPath(job, new Path(args[1])); // Où sont les fichiers de sortie
		
		// Attente de la complétion de tous les calculs avant d'exit le programme
		return job.waitForCompletion(true) ? -1 : 0;
	}

	public static void main(String[] args) throws Exception {
		// La classe du dessus sera lancé puisqu'elle implémente Tool
		// On ne se soucie pas des exceptions ici, puisque chaque erreur sera Catché dans le Mapper ou le Reducer
		System.exit(ToolRunner.run(new Configuration(), new LocationClientDriver(), args));

	}

}
