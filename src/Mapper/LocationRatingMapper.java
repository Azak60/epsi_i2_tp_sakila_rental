package Mapper;
import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class LocationRatingMapper extends Mapper<LongWritable, Text, Text, FloatWritable> {
	@Override
	protected void map(LongWritable lineNumber, Text line, Mapper<LongWritable, Text, Text, FloatWritable>.Context context) throws IOException, InterruptedException {
	
		String[] fields = line.toString().split(";");
		
		try {
			String rating = fields[24];
			
			String amountTmp = fields[2].replaceAll("\"", "");
			float amount = Float.parseFloat(amountTmp);
			
			context.write(new Text(rating), new FloatWritable(amount));
		}
		catch(Exception e) {
			context.getCounter("Errors", e.getMessage()).increment(1);
		}
	}
}