package Mapper;
import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class LocationClientMapper extends Mapper<LongWritable, Text, Text, FloatWritable> {
	@Override
	protected void map(LongWritable lineNumber, Text line, Mapper<LongWritable, Text, Text, FloatWritable>.Context context) throws IOException, InterruptedException {
	
		String[] fields = line.toString().split(";");
		
		try {
			String email = fields[5];
			String amountTmp = fields[2].replaceAll("\"", "");
			
			float amount = Float.parseFloat(amountTmp);
			context.write(new Text(email), new FloatWritable(amount));
		}
		catch(Exception e) {
			context.getCounter("Errors", e.getMessage()).increment(1);
		}
	}
}
