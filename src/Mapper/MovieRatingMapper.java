package Mapper;
import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class MovieRatingMapper extends Mapper<LongWritable, Text, Text, Text > {
	@Override
	protected void map(LongWritable lineNumber, Text line, Mapper<LongWritable, Text, Text, Text>.Context context) throws IOException, InterruptedException {
	
		String[] fields = line.toString().split(";");
		
		try {
			String title = fields[17];
			String rating = fields[24];
			context.write(new Text(title), new Text(rating));
		}
		catch(Exception e) {
			context.getCounter("Errors", e.getMessage()).increment(1);
		}
	}
}
